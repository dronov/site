---
layout: default
permalink: helpfulstuff/
---

# Helpful stuff

Меню:

 - [Swiss-knife OS](#swiss-knife)
 - [Exploits](#exploits)
 - [Seach engines](#search-engines)
 - [OSINT](#osint)
 - [All in one tools](#tools)
 - [Git](#git)
 - [Traffic](#traffic)
 - [Security Tools](#security-tools)
 - [Mobile Android](#mobile-android)
 - [Mobile iOS](#mobile-ios)
 - [Metasploit](#metasploit)
 - [Wifi](#wifi)
 - [SS7](#ss7)
 - [XSS, SQLi, RCE](#xss-sqli-rce)
 - [Hash crackers](#hash-crackers)
 - [Some cool online stores](#some-cool-online-stores)

## <a name="swiss-knife"></a>Swiss-knife OS

- [Kali Linux](https://www.kali.org) представляет из себя дистрибутив, содержащий множество утилит для проведения тестирования на проникновение: от анализа уязвимостей веб-приложений, до взлома сетей и сервисов и закрепления в системе.
- [BackBox](https://backbox.org) is more than an operating system, it is a Free Open Source Community project with the aim to promote the culture of security in IT environment and give its contribute to make it better and safer. All this using exclusively Free Open Source Software by demonstrating the potential and power of the community. 
- [PentestBox](https://pentestbox.org) не похож на другие security-дистрибутивы, которые работают на виртуальных машинах. Это архив с программами под Windows. Качаем, распаковывем и работаем.

## <a name="exploits"></a>Exploits

- [The Exploit Database](https://www.exploit-db.com/) - Exploits, Shellcode, 0days, Remote Exploits, Local Exploits, Web Apps, Vulnerability Reports, Security Articles, Tutorials and more.

## <a name="search-engines"></a>Seach engines 

- [Vulners](https://vulners.com) - это очень большая и непрерывно обновляемая база данных ИБ-контента. Сайт позволяет искать уязвимости, эксплоиты, патчи, результаты bug bounty так же, как обычный поисковик ищет сайты. [тут тоже очень круто про него написано](https://xakep.ru/2016/07/08/vulners)
- [Shodan](https://www.shodan.io) is the world's first search engine for Internet-connected devices.
- [Censys](https://censys.io) is a search engine that allows computer scientists to ask questions about the devices and networks that compose the Internet. 
- [GrayHatWarfare](https://buckets.grayhatwarfare.com/). Public buckets.
- [GreyNoise Visualizer](https://viz.greynoise.io/table)

## <a name="osint"></a>OSINT

- [OSINT tools list](https://start.me/p/wMdQMQ/tools). On this page you’ll find tools which you can help do your OSINT reseach.

## <a name="tools"></a>All in one tools

- [Sn1per](https://github.com/1N3/Sn1per) - is an automated scanner that can be used during a penetration test to enumerate and scan for vulnerabilities

## <a name="git"></a>Git

- [Git Magic](http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/en/) - это лучший, по моему мнению, учебник по контролю версий git 
- [GitLab](https://gitlab.com) - бесплатный и хороший сервис контроля версий с pages, pipeline, CI, wiki и прочими ништяками

## <a name="traffic"></a>Network

- [bettercap](https://www.bettercap.org) is the Swiss army knife for network attacks and monitoring. 
- [Mitmproxy](https://mitmproxy.org) an interactive console program that allows traffic flows to be intercepted, inspected, modified and replayed. Написана на пятоне и, соответвенно, хорошо, что позволяет вертеть ее как угодно :)

## <a name="security-tools"></a>Security Tools

- [Burp Suite](https://portswigger.net/burp/), the leading toolkit for web application security testing
- [OWASP Zed Attack Proxy(ZAP)](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project) is one of the world’s most popular free security tools and is actively maintained by hundreds of international volunteers*. It can help you automatically find security vulnerabilities in your web applications while you are developing and testing your applications. Its also a great tool for experienced pentesters to use for manual security testing.


## <a name="mobile-android"></a>Mobile Android

- [Find Security Bugs](https://find-sec-bugs.github.io) for security audits of Java web applications.
- [Drozer](https://github.com/mwrlabs/drozer) Comprehensive security and attack framework for Android. 
- [Inspeckage](http://ac-pm.github.io/Inspeckage/) is a tool developed to offer dynamic analysis of Android applications. By applying hooks to functions of the Android API, Inspeckage will help you understand what an Android application is doing at runtime.

## <a name="mobile-ios"></a>Mobile iOS

- [Needle](https://github.com/mwrlabs/needle) The iOS Security Testing Framework.

## <a name="metasploit"></a>Metasplot

- [Metasplot](https://www.rapid7.com/products/metasploit/download/pro/thank-you/) World's most used penetration testing software
- [Metasploitable](https://sourceforge.net/projects/metasploitable/) is an intentionally vulnerable Linux virtual machine. This VM can be used to conduct security training, test security tools, and practice common penetration testing techniques. 
- [Metasploit Unleashed by Offensive Security](https://www.offensive-security.com/metasploit-unleashed/) The ultimate guide to the Metasploit Framework

## <a name="wifi"></a>WiFi

- [WiFi arsenal](https://github.com/0x90/wifi-arsenal) is a pack of various usefull/useless tools for 802.11 hacking.

## <a name="ss7"></a>SS7

- [SS7](https://github.com/0x90/ss7-arsenal) tools and scripts

## <a name="xss-sqli-rce"></a>XSS, SQLi, etc..

- [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings) A list of useful payloads and bypass for Web Application Security and Pentest/CTF
- [The Big List of Naughty Strings](https://github.com/minimaxir/big-list-of-naughty-strings) is a list of strings which have a high probability of causing issues when used as user-input data.
- [XSS payload](https://gitlab.com/deadroot/xss.js) for steal some data

## <a name="hash-crackers"></a>Hash crackers and Wordlist`s

- [Various Online Password Crackers](http://carnal0wnage.attackresearch.com/2010/01/various-online-password-crackers.html) Just a list of online (mostly) md5 crackers but some with do others.
- [HashKiller's](https://hashkiller.co.uk) purpose is to serve as a meeting place for computer hobbyists, security researchers and penetration testers. It serves as a central location to promote greater security on the internet by demonstrating the weakness of using hash based storage / authentication.
- [GPUHASH.me](https://gpuhash.me/) - online WPA/WPA2 hash cracker.
- [xsrc.ru](https://xsrc.ru/) - Восстановление паролей от Wi-Fi из хендшейка
- [Wordlists](https://github.com/berzerk0/Probable-Wordlists) sorted by probability originally created for password generation and testing

## <a name="some-cool-online-stores"></a>Some cool online stores

- [Hakshop](https://hakshop.com) the premiere store of Hak5. Home to exclusive hacking equipment, award winning media and immersive information security training.
- [Great Scott Gadgets](https://greatscottgadgets.com) open source hardware for innovative people
