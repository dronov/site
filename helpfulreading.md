---
layout: default
permalink: helpfulreading/
---

# Helpful stuff

Меню:

 - [Telegram](#telegram)
 - [Форумы](#forums)
 - [Блоги](#blogs)

## <a name="telegram"></a>Telegram чаты и каналы
 - [Новостной агрегатор от DC20e6](https://t.me/defcon_news) Агрегатор новостей из мира информационной безопасности (кучи разных мест). Да, тут огромное количество сообщений постоянно :). [Список RSS](https://pastebin.com/P2j2dGcT).
 - [Информация опасносте](https://t.me/alexmakus). Чаще про cybersex (зачеркнуто) про cyberseс.

## <a name="forums"></a>Форумы

- [rdot.org](https://rdot.org/forum/) keywords: аспекты несанкционированного доступа, компьютерная безопасность, уязвимости, администрирование, программирование.
- [codeby.net](https://codeby.net/) Информационная безопасность и защита информации компьютерных сетей. Программирование, этичный хакинг и тестирование на проникновение, форензика.

## <a name="blogs"></a>Блоги

- [bo0om - Explosive blog](https://bo0om.ru/en/) keywords: кавычки и веселье
- [Бизнес без опасности](https://lukatsky.blogspot.com/) Бумажная безопасность или "Бизнес безопасности или безопасность бизнеса? О том и о другом в одном блоге..."

