---
layout: post
title: defcon_news
date: 2018-12-11 00:00:00
categories: projet
short_description: Агрегатор новостей из мира информационной безопасности (кучи разных мест).
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---


Меня иногда спрашивают какие каналы я читаю и вообще откуда новости получаю, на что ответить что-то вроде "100500 блогов, твиторов, лент уязвимостей и прочего" не является хорошим тоном (потому что считают что я посылаю лесом:))

Как результат я собрал большинство каналов типа твиторов, блогов и RSS в канал (@defcon_news)[https://t.me/defcon_news] и там, если что, можно смотреть и читать. Да, там огромное количество сообщений постоянно :) Могу только посоветовать отключить оповещения и потом читать ;)

Если хотите сами агрегировать, то ниже список RSS

```
DEF CON Announcements!(https://www.defcon.org/) -> http://www.defcon.org/defconrss.xml (RSS)
HackerOne Blog(https://www.hackerone.com/blog) -> https://www.hackerone.com/blog.rss (RSS)
Hacking, AppSec, and Bug Bounty newsletter(https://www.hackerone.com/zerodaily) -> https://www.hackerone.com/zerodaily.rss (RSS)
Blaze's Security Blog(https://bartblaze.blogspot.com/) -> https://bartblaze.blogspot.com/feeds/posts/default?alt=rss (RSS)
AlienVult Labs Blog(https://www.alienvault.com/blogs/labs-research) -> http://feeds.feedblitz.com/alienvaultotx&x=1 (RSS)
Dynamoo's Blog(https://blog.dynamoo.com/) -> https://blog.dynamoo.com/feeds/posts/default?alt=rss (RSS)
Mozilla Hacks – The Web Developer Blog(https://hacks.mozilla.org/) -> https://hacks.mozilla.org/feed/ (RSS)
Project Zero(https://googleprojectzero.blogspot.com/) -> https://googleprojectzero.blogspot.com/feeds/posts/default?alt=rss (RSS)
/r/netsec - Information Security News & Discussion(https://www.reddit.com/r/netsec/) -> http://www.reddit.com/r/netsec/.rss (RSS)
Kali Linux(https://www.kali.org) -> http://www.kali.org/feed/ (RSS)
KitPloit - PenTest Tools!(http://www.kitploit.com/) -> http://feeds.feedburner.com/PentestTools (RSS)
Ethical Hacking Tutorials(https://www.ethicalhackx.com) -> http://www.ethicalhackx.com/feed/ (RSS)
pwned(https://www.reddit.com/r/pwned/) -> http://www.reddit.com/r/pwned/.rss (RSS)
TrendLabs Security Intelligence Blog(https://blog.trendmicro.com/trendlabs-security-intelligence) -> http://feeds.trendmicro.com/Anti-MalwareBlog/ (RSS)
Black Hills Information Security(https://www.blackhillsinfosec.com) -> http://www.blackhillsinfosec.com/?feed=rss2 (RSS)
Schneier on Security(https://www.schneier.com/blog/) -> http://www.schneier.com/blog/index.rdf (RSS)
Null Byte « WonderHowTo(https://null-byte.wonderhowto.com/) -> http://null-byte.wonderhowto.com/rss.xml (RSS)
Malware Analysis & Reports(https://www.reddit.com/r/Malware/) -> http://www.reddit.com/r/Malware/.rss (RSS)
Dark Reading(https://www.darkreading.com) -> http://www.darkreading.com/rss/all.xml (RSS)
SecureNews(https://securenews.ru) -> https://securenews.ru/feed/ (RSS)
UpGuard Blog(https://www.upguard.com/blog) -> http://www.scriptrock.com/blog/rss.xml (RSS)
SANS Internet Storm Center, InfoCON: green(https://isc.sans.edu) -> http://iscxml.sans.org/rssfeed.xml (RSS)
CyberPunk(https://n0where.net) -> http://n0where.net/feed/ (RSS)
Exploit Collector(http://exploit.kitploit.com/) -> http://exploit.kitploit.com/feeds/posts/default (RSS)
AWS DevOps Blog(https://aws.amazon.com/blogs/devops/) -> http://blogs.aws.amazon.com/application-management/blog/feed/recentPosts.rss (RSS)
The Hacker News(https://thehackernews.com/) -> http://thehackernews.com/feeds/posts/default (RSS)
hckAlert(https://www.hckalert.com/) -> https://www.hckalert.com/feeds/posts/default (RSS)
Embedded Lab(http://embedded-lab.com/blog) -> http://embedded-lab.com/blog/?feed=rss2 (RSS)
Liquidmatrix Security Digest(https://www.liquidmatrix.org/blog) -> http://feeds.feedburner.com/Liquidmatrix (RSS)
Malwarebytes Labs(https://blog.malwarebytes.com) -> http://blog.malwarebytes.org/feed/ (RSS)
Legal Hacker(https://legalhackers.com/)) -> http://rss.ricterz.me/legalhackers (RSS)
Hakin9 – IT Security Magazine(https://hakin9.org) -> http://hakin9.org/feed/ (RSS)
Have I Been Pwned latest breaches(https://haveibeenpwned.com/) -> http://feeds.feedburner.com/HaveIBeenPwnedLatestBreaches (RSS)
Форум специалистов по информационной безопасности - codeby.net(https://codeby.net/) -> https://codeby.net/articles/index.rss (RSS)
Theori(https://theori.io/) -> http://www.theori.io/feed.xml (RSS)
ToolsWatch.org – The Hackers Arsenal Tools Portal(http://www.toolswatch.org) -> http://www.toolswatch.org/feed/ (RSS)
Exploit Files ≈ Packet Storm(https://packetstormsecurity.com/) -> http://packetstormsecurity.org/exploits.xml (RSS)
Full Disclosure(http://seclists.org/#fulldisclosure) -> http://seclists.org/rss/fulldisclosure.rss (RSS)
Packet Storm Security(https://packetstormsecurity.com/) -> http://packetstormsecurity.org/headlines.xml (RSS)
Dangerous Prototypes(http://dangerousprototypes.com/blog) -> http://dangerousprototypes.com/feed/ (RSS)
Latest Hacking News(https://latesthackingnews.com) -> http://www.latesthackingnews.com/feed/ (RSS)
Imperva Blog(https://www.imperva.com/blog) -> http://feeds.feedburner.com/Imperviews?format=xml https://www.imperva.com/blog
Fire Eye Threat Research(http://www.fireeye.com/blog/threat-research.html) -> http://www.fireeye.com/blog/feed (RSS)
Talos Blog(https://blog.talosintelligence.com/) -> http://vrt-sourcefire.blogspot.com/feeds/posts/default (RSS)
Vulners.com RSS Feed(https://vulners.com) -> https://vulners.com/rss.xml?query=type:hackerone (RSS)
OpenNews.opennet.ru: Проблемы безопасности(https://www.opennet.ru/opennews/index.shtml?f=vuln) -> http://www.opennet.ru/opennews/opennews_sec.rss (RSS)
AWS Security Blog(https://aws.amazon.com/blogs/security/) -> http://blogs.aws.amazon.com/security/blog/feed/recentPosts.rss (RSS)
«Хакер»(https://xakep.ru) -> http://www.xakep.ru/articles/rss/default.asp (RSS)
Krebs on Security(https://krebsonsecurity.com) -> http://krebsonsecurity.com/feed/ (RSS)
0day.today(http://0day.today/) -> http://0day.today/rss (RSS)
WPScan Vulnerability Database(https://wpvulndb.com) -> https://wpvulndb.com/feed.xml (RSS)
The PhishLabs Blog(https://info.phishlabs.com/blog) -> http://blog.phishlabs.com/rss.xml (RSS)
SparkFun: Commerce Blog(https://www.sparkfun.com/news) -> http://www.sparkfun.com/commerce/rss.php (RSS)
Google Online Security Blog(http://security.googleblog.com/) -> http://googleonlinesecurity.blogspot.com/atom.xml (RSS)
Seeed Studio Blog(http://www.seeedstudio.com/blog) -> http://www.seeedstudio.com/blog/feed/ (RSS)
Exploit.IN(https://exploit.in) -> http://exploit.in/rss.php (RSS)
US-CERT Current Activity(https://www.us-cert.gov/ncas/current-activity.xml) -> http://www.us-cert.gov/current/index.rdf (RSS)
Naked Security(https://nakedsecurity.sophos.com) -> http://nakedsecurity.sophos.com/feed/ (RSS)
Информационная безопасность – Защита данных(https://habr.com/ru/hub/infosecurity/all/) -> http://habrahabr.ru/rss/blogs/infosecurity/ (RSS)
Sucuri Blog(https://blog.sucuri.net) -> http://blog.sucuri.net/category/wordpress-security/feed (RSS)
Infosec Writers Club(https://www.secjuice.com/) -> https://www.secjuice.com/rss/ (RSS)
Threatpost(https://threatpost.ru) -> http://threatpost.ru/feed/ (RSS)
```

---
Sincerely yours,
Roman Ananev